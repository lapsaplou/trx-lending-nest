import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { AddressController } from './address.controller';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lending } from 'src/lending/lending.entity';
import { Address } from './address.entity';
import { LendingModule } from 'src/lending/lending.module';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Address])],
  controllers: [AddressController],
  providers: [AddressService]
})
export class AddressModule {}
