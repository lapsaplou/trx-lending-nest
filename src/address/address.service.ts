import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { response } from 'express';
import { createQueryBuilder, getConnection, getRepository, Repository } from 'typeorm';
import { Address } from './address.entity';
import { CreateAddressDto } from './dto/create-address.dto';
import { CreateFreezeDto } from './dto/create-freeze.dto';
import { TransferDto } from './dto/transfer.dto';
import { UpdateAddressDto } from './dto/update-address.dto';

const TronWeb = require('tronweb')
const HttpProvider = TronWeb.providers.HttpProvider;
// const tronUrl = 'https://api.tronstack.io';
const testUrl = 'https://api.nileex.io';
const fullNode = new HttpProvider(testUrl);
const solidityNode = new HttpProvider(testUrl);
const eventServer = new HttpProvider(testUrl);

//  AAAAAAAAAAA's wallet
// const wallet_address = "TEzyFay8XUt7yqxfwx2AiEBHhmZVJyMvUH";
// const privateKey = "7ce97f78624dd8bcf96e2733527aa5f6c7eaec898dd8342e37bf0394802afd5c";
// const wallet_address = "TTyvqRyqH8PA9FtdPxahchdPyLxm8aePQB";
// const privateKey = "86baf7343ad5045eb9b3313041847677385fd7a2ddb6f76b4aa7becfdc1e8044";
const wallet_address = "TJL7uKZhyXT8TKoTj2RRaEg9xjfqG3vxqk";
const privateKey = "993c07a2a22e667a4a5dc5201c8a576fb559fc53b6de41f49f37068d9658317c";
const tronWeb = new TronWeb(fullNode, solidityNode, eventServer);


@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(Address)
    private addressEntity: Repository<Address>) { }

  async findAll() {
    const results = await this.addressEntity.find({
      order: {
        created_at: "DESC"
      }
    });
    const allAddress = [];
    for (let i = 0; i < results.length; i++) {
      const energy = await this.getAccount(results[i]['address']);
      allAddress[i] = { ...results[i], ...energy };
    }
    return allAddress;
  }

  async findOne(tag: string) {
    const result = await this.getAddressByTag(tag);
    const account = await this.getAccount(result['address']);
    return { ...result, ...account };
  }

  create(createAddressDto: CreateAddressDto) {
    return 'This action adds a new address';
  }

  update(id: number, updateAddressDto: UpdateAddressDto) {
    return `This action updates a #${id} address`;
  }

  remove(id: number) {
    return `This action removes a #${id} address`;
  }

  async getAddressByTag(tag: string) {
    return await this.addressEntity.findOne({
      where: { tag: tag }
    });
  }

  async getAccount(address) {
    const account = await tronWeb.trx.getAccountResources(address)
      .then(result => { return result })
    // const energyPerTRX = (90000000000 / account.TotalEnergyWeight).toFixed(4);
    const energyGauge = (1 - ((account.EnergyUsed ? account.EnergyUsed : 0) / account.EnergyLimit)).toFixed(4);
    const energyLeft = account.EnergyLimit - (account.EnergyUsed ? account.EnergyUsed : 0);
    return { energyGauge: energyGauge, energyLimit: account.EnergyLimit, energyLeft: energyLeft };
  }

  async freezeBalance(dto: CreateFreezeDto) {
    try {
      let transaction;
      const axios = require('axios');
      await axios({
        method: 'post',
        url: 'https://nile.trongrid.io/wallet/freezebalance',
        data: {
          "owner_address": tronWeb.address.toHex(wallet_address),
          "frozen_balance": dto.frozen_balance,
          "frozen_duration": dto.frozen_duration,
          "resource": "ENERGY"
        },
        headers: {}
      }).then(function (response) {
        transaction = response.data;
      }).catch(function (error) {
        console.log(error);
      });;

      let signed = await tronWeb.trx.sign(transaction, privateKey);

      // let broadcast = await tronWeb.trx.sendRawTransaction(signed);
      const post = {
        method: 'POST',
        url: 'https://nile.trongrid.io/wallet/broadcasttransaction',
        headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
        data: {
          raw_data: signed.raw_data,
          raw_data_hex: signed.raw_data_hex,
          signature: signed.signature
        }
      };
      axios.request(post).then(function (response) {
        console.log('yay');
        console.log(response.data);
      }).catch(function (error) {
        console.log('nay');
        console.error(error);
      });

    } catch (error) {
      return error;
    }
  }

  async unfreezeBalance() {
    try {
      let transaction;
      const axios = require('axios');
      await axios({
        method: 'post',
        url: 'https://api.shasta.trongrid.io/wallet/unfreezebalance',
        data: {
          "owner_address": tronWeb.address.toHex(wallet_address),
          "resource": "ENERGY"
        },
        headers: {
        }
      }).then(function (response) {
        transaction = response.data;
      }).catch(function (error) {
        console.log(error);
      });;

      let signed = await tronWeb.trx.sign(transaction, privateKey);

      // let broadcast = await tronWeb.trx.sendRawTransaction(signed);
      const post = {
        method: 'POST',
        url: 'https://nile.trongrid.io/wallet/broadcasttransaction',
        headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
        data: {
          raw_data: signed.raw_data,
          raw_data_hex: signed.raw_data_hex,
          signature: signed.signature
        }
      };
      axios.request(post).then(function (response) {
        console.log('yay');
        console.log(response.data);
      }).catch(function (error) {
        console.log('nay');
        console.error(error);
      });

    } catch (error) {
      return error;
    }
  }

  async tranferTrx(dto:TransferDto){
    const axios = require('axios');
    console.log(dto.to_address);
    console.log(dto.amount);
    console.log(wallet_address);
    let transaction = await tronWeb.transactionBuilder.sendTrx(dto.to_address, dto.amount, wallet_address);
    let signed = await tronWeb.trx.sign(transaction, privateKey);
    const post = {
      method: 'POST',
      url: 'https://nile.trongrid.io/wallet/broadcasttransaction',
      headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
      data: {
        raw_data: signed.raw_data,
        raw_data_hex: signed.raw_data_hex,
        signature: signed.signature
      }
    };
    axios.request(post).then(function (response) {
      console.log('yay');
      console.log(response.data);
    }).catch(function (error) {
      console.log('nay');
      console.error(error);
    });
  }


}
