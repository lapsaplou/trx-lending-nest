export class CreateAddressDto {
    address: string;
    tag: string;
    balance: number;
    energy_max: number;
}
