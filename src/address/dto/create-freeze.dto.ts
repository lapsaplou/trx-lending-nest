export class CreateFreezeDto {
    frozen_balance: number;
    frozen_duration: number;
}
