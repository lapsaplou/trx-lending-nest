import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AddressService } from './address.service';
import { CreateAddressDto } from './dto/create-address.dto';
import { CreateFreezeDto } from './dto/create-freeze.dto';
import { TransferDto } from './dto/transfer.dto';
import { UpdateAddressDto } from './dto/update-address.dto';

@Controller('address')
export class AddressController {
  constructor(private readonly addressService: AddressService) {}

  @Post()
  create(@Body() createAddressDto: CreateAddressDto) {
    return this.addressService.create(createAddressDto);
  }

  @Get()
  async findAll() {
    return await this.addressService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.addressService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAddressDto: UpdateAddressDto) {
    return this.addressService.update(+id, updateAddressDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.addressService.remove(+id);
  }

  @Post('freezeBalance')
  freezeBlance(@Body() CreateFreezeDto: CreateFreezeDto) {
    return this.addressService.freezeBalance(CreateFreezeDto);
  }

  @Post('unfreezeBalance')
  unfreezeBlance() {
    return this.addressService.unfreezeBalance();
  }

  @Post('transfer')
  transferTrx(@Body() TransferDto: TransferDto) {
    return this.addressService.tranferTrx(TransferDto);
  }

  
}
