import { Deposit } from 'src/deposit/deposit.entity';
import { Lending } from 'src/lending/lending.entity';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinTable, JoinColumn, OneToMany } from 'typeorm';

export enum Status {
    COMPLETED = "completed",
    ONGOING = "ongoing",
    CANCELED = "canceled"
}

@Entity('address')
export class Address {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 200 })
    address: string;

    // @OneToMany(() => Lending, lending => lending.address,{
    //     cascade: true
    // })
    // @JoinColumn()
    // lendings: Lending[];

    // @ManyToOne(() => Deposit, deposit => deposit.addresses,{
    //     cascade: ["update"]
    // })
    // @JoinTable()
    // deposits: Deposit[];

    @Column("varchar", { length: 200 })
    tag: string;

    @Column("decimal", { precision: 10, scale: 2 })
    balance: number;

    @Column("bigint")
    energy_max: number;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

}