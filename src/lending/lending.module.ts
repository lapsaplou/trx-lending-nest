import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule, InjectRepository } from '@nestjs/typeorm';
import { LendingController } from './lending.controller';
import { LendingService } from './lending.service';
import { Lending } from './lending.entity';
import { Address } from 'src/address/address.entity';
import { AddressModule } from 'src/address/address.module';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Lending,Address]), AddressModule],
  controllers: [LendingController],
  providers: [LendingService],
})
export class LendingModule {}
