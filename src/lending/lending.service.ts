import { Injectable, Param } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Address } from 'src/address/address.entity';
import { Repository } from 'typeorm';
import { CreateLendingDto } from './dto/create-lending.dto';
import { UpdateLendingDto } from './dto/update-lending.dto';
import { Lending } from './lending.entity';

const TronWeb = require('tronweb')
const HttpProvider = TronWeb.providers.HttpProvider;
const tronUrl = 'https://api.tronstack.io';
const testUrl = 'https://api.nileex.io';
const fullNode = new HttpProvider(tronUrl);
const solidityNode = new HttpProvider(tronUrl);
const eventServer = new HttpProvider(tronUrl);
const privateKey = "eb4355efccd6fc789c5a7445698001976d7e3f1695aa9ad9a8d5fdc0cd29b970";
const tronWeb = new TronWeb(fullNode,solidityNode,eventServer,privateKey);

const trc20ContractAddress = "TPgbgZReSnPnJeXPakHcionXzsGk6kVqZB";//Total Energy Address
@Injectable()
export class LendingService {
    constructor(
        @InjectRepository(Lending)
        private lendingRepository: Repository<Lending>,
        @InjectRepository(Address)
        private addressRepository: Repository<Address>
        // private lendingEntity : Lending
    ) {}

    async findAll() {
    // async findAll(): Promise<Lending[]> {
        // var passwordHash = require('password-hash');
        // var hashedPassword = passwordHash.generate('password123');
        // return hashedPassword;
        return await this.lendingRepository.find({
            relations:['address'],
            order: {
				created_at: "DESC"
			}
        });

    }

    
    async findOne(address: string) {
        return await this.lendingRepository.find({
            where:{receiving_address: address}
        });
    }

    async create( createLendingDto: CreateLendingDto) {
        const receivingAddress = await this.addressRepository.find({
            where:{address: createLendingDto.receiving_address}
        });
        const newBalance = receivingAddress[0].balance - createLendingDto.spent - createLendingDto.fees;
        await this.addressRepository.update({
            address : createLendingDto.receiving_address
        },{
            balance:  newBalance
        });
        return await this.lendingRepository.save({ ...createLendingDto});
    }

    update(id: number, updateAddressDto: UpdateLendingDto) {
        return `This action updates a #${id} address`;
    }

    remove(id: number) {
        return `This action removes a #${id} address`;
    }

    async getLastestID() {
        try {
            const contract = await tronWeb.contract().at(trc20ContractAddress);
            const result = await contract.orderID().call();
            return parseInt(result, 10);
        } catch(error) {
            console.error("trigger smart contract error",error)
        }
    }

    async lendingOrder(energy, days, address) {
        try {
            const contract = await tronWeb.contract().at(trc20ContractAddress);
            const stakingEfficiency = this.energyPerTRX();
            const TRXtoStake =  Math.ceil(energy/await (stakingEfficiency));
            if(TRXtoStake < 1000 ||  days >30 || days <3){
                return 'TRXamount too low'
            }

            //include markup for contract, divide 100k per energy and times 1000k for trx hex value
            const TRXpayable = Math.ceil(energy * days * 84); // 5% * 8Trx / per100k * 1000000 (decimals) 
            const trxAmount = (TRXtoStake * 1000000).toString();
            const hexTrx = parseInt(trxAmount, 10);
            const hexDay = parseInt(days, 10);
            const callLending = contract.entrustOrder(hexTrx, hexDay, address)
                .send({
                    callValue: TRXpayable,
                    feeLimit: 1000000000,
                    shouldPollResponse: true
                });
            return callLending;
        } catch(error) {
            console.error("trigger smart contract error",error)
        }
    }

    async getAccount(address) {
        const account = await tronWeb.trx.getAccountResources(address)
            .then(result => {return result})
        const energyPerTRX = (90000000000 / account.TotalEnergyWeight).toFixed(4);
        const energyGauge = (1 - (account.EnergyUsed /account.EnergyLimit)).toFixed(4);
        return {energyGauge: energyGauge, energyPerTRX: energyPerTRX};
    }

    async energyPerTRX() {
        const account = await tronWeb.trx.getAccountResources('TModG6zcWvss8CFpHoTDf18LbFBBYKFfby')
            .then(result => {return result})
        return (90000000000 / account.TotalEnergyWeight);
    }
}