import { Address } from 'src/address/address.entity';
import { Entity, Column, OneToMany, PrimaryGeneratedColumn, JoinTable, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, PrimaryColumn } from 'typeorm';

@Entity('lending')
export class Lending {
    @PrimaryGeneratedColumn()
    id: number;

    @PrimaryColumn("varchar", { length: 200 })
    receiving_address: string;

    // @OneToMany(() => Address, address => address.lendings,{
    //     cascade: ["update"]
    // })
    // @JoinTable()
    // addresses: Address[];

    @Column("varchar", { length: 200 })
    staking_address: string;

    @Column("bigint")
    freeze: number;

    @Column("bigint")
    energy: number;

    @Column("decimal", { precision: 10, scale: 2 })
    spent: number;

    @Column({
        type: "enum",
        enum: ['tronlend', 'tokengoodie', 'telegram'],
        default: 'telegram'
    })
    source: string;

    @Column("varchar", { length: 200, unique: true })
    hash: string;

    @Column({
        type: "enum",
        enum: ['ongoing', 'completed', 'scammed', 'failed'],
        default: 'ongoing'
    })
    status: string;

    @Column({
        type: "smallint",
        default: 3
    })
    duration: number;

    @Column("decimal", { precision: 10, scale: 2 })
    fees: number;

    @Column({
        type: "enum",
        enum: ['energy', 'bandwith'],
        default: 'energy'
    })
    power: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @ManyToOne(() => Address, (address) => address.address)
    @JoinColumn({name: 'receiving_address', referencedColumnName:'address'} )
    address: Address;
}