
import { Controller, Get, Post, Param, HttpCode, ParseIntPipe, HttpStatus, HttpException, UseInterceptors, Patch, Body, Delete, UseGuards} from '@nestjs/common';
import { LendingService } from './lending.service';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse, AxiosRequestConfig } from 'axios';
import { ForbiddenException } from '../forbidden.exception';
import { Lending } from './interfaces/lending.interface';
import { Observable} from 'rxjs/internal/Observable';
import { response } from 'express';
import { resourceLimits } from 'worker_threads';
import console from 'console';
import { LockNotSupportedOnGivenDriverError } from 'typeorm';
import { UpdateLendingDto } from './dto/update-lending.dto';
import { CreateLendingDto } from './dto/create-lending.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
// import { AxiosResponse } from '@nestjs/common/http/interfaces/axios.interfaces';
// import fetch from "node-fetch";


@Controller('lending')
export class LendingController {
    constructor(private lendingService: LendingService, private httpService: HttpService) {}
    @UseGuards(JwtAuthGuard)
    @Get(':id')
    async findOne(@Param('id') id: string) {
        return await this.lendingService.findOne(id);
    }

    // @UseGuards(JwtAuthGuard)
    @Get()
    @HttpCode(202)
    async findAll() {
        return await this.lendingService.findAll();
    }

    @UseGuards(JwtAuthGuard)
    @Post()
    async create(@Body() createLendingDto : CreateLendingDto) {
        // return {createLendingDto};
        return await this.lendingService.create(createLendingDto);
    }

    @Patch(':id')
    update(@Param('id') id: string, @Body() updateAddressDto: UpdateLendingDto) {
        return this.lendingService.update(+id, updateAddressDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.lendingService.remove(+id);
    }

}