export class CreateLendingDto {
    // id: number;
    receiving_address: string;
    staking_address: string;
    freeze: number;
    spent: number;
    source: string;
    energy: number;
    hash: string;
    duration: number;
    status: string;
    fees: number;
    power: string;
    // created_at: Date;
    // updated_at: Date;
}
