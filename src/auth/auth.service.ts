import { Body, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) {}

    async validateUser(@Body() username: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne(username);
        const bcrypt = require('bcrypt');
        const validPassword = await bcrypt.compare(pass, user.password);
        if (user && validPassword) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async register(user: any) {
        const bcrypt = require('bcrypt');
        const salt = await bcrypt.genSalt(10);
        const hashed = await bcrypt.hash(user.password, salt);
        await this.usersService.register({'email': user.username, 'password': hashed})
    }

    async login(user: any) {
        const payload = { username: user.username, sub: user.userId };
        return {
            access_token: this.jwtService.sign(payload,{expiresIn:60*60*24}),
        };
    }
}