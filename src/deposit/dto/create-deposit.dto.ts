export class CreateDepositDto {
    from_address: string;
    to_address: string;
    amount: number;
    hash: string;
    status: string;
}
