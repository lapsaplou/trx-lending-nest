import { Address } from 'src/address/address.entity';
import { Entity, Column, ManyToOne, PrimaryGeneratedColumn, JoinColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity('deposit')
export class Deposit {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 200 })
    from_address: string;

    // @OneToMany(() => Address, address => address.deposits,{
    //     cascade: ["update"]
    // })
    // @JoinTable()
    // addresses: Address[];

    @Column("varchar", { length: 200 })
    to_address: string;

    @Column("decimal", { precision: 10, scale: 2 })
    amount: number;

    @Column("varchar", { length: 200, unique: true })
    hash: string;

    @Column({
        type: 'enum',
        enum: ['success', 'failed'],
        default: 'success'
    })
    status: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @ManyToOne(() => Address, (address) => address.address)
    @JoinColumn({name: 'to_address', referencedColumnName:'address'} )
    address: Address;
}