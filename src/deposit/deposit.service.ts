import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Address } from 'src/address/address.entity';
import { CreateDepositDto } from './dto/create-deposit.dto';
import { UpdateDepositDto } from './dto/update-deposit.dto';
import { Deposit } from './deposit.entity';

@Injectable()
export class DepositService {
	constructor(
		@InjectRepository(Deposit)
		private depositRepository: Repository<Deposit>,
		@InjectRepository(Address)
		private addressRepository: Repository<Address>
	) {}

	async create(createDepositDto: CreateDepositDto) {
		const receivingAddress = await this.addressRepository.find({
			where:{address:createDepositDto.to_address}
		});
		const newBalance = Number(receivingAddress[0].balance) + Number(createDepositDto.amount);
		await this.addressRepository.update({
			address: createDepositDto.to_address
		},{
			balance: newBalance
		});
		return await this.depositRepository.save({...createDepositDto});
    }

	async findAll() {
        return await this.depositRepository.find({
			relations:['address'],
			order: {
				created_at: "DESC"
			}
		});
	}

	findOne(id: number) {
		return `This action returns a #${id} deposit`;
	}

	update(id: number, updateDepositDto: UpdateDepositDto) {
		return `This action updates a #${id} deposit`;
	}

	remove(id: number) {
		return `This action removes a #${id} deposit`;
	}
}
