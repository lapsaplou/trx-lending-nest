import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { LocalAuthGuard } from './auth/local-auth.guard';

@Controller()
export class AppController {
  // constructor(private readonly appService: AppService) {}
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() user) {
    return this.authService.login(user);
  }

  @Post('auth/register')
  async register(@Body() user) {
    return this.authService.register(user);
  }

  @Get()
  getHello(): string {
    return 'Hello World!';

    // return this.appService.getHello();
  }
}
