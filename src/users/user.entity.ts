import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

export enum Role {
    ADMIN = "admin",
}

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 200, unique: true })
    email: string;

    @Column("varchar", { length: 200 })
    password: string;

    @Column({
        type: "set",
        enum: Role,
        default: [Role.ADMIN]
    })
    role: Role[];

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

}