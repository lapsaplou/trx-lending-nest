import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LendingController } from './lending/lending.controller';
import { LendingService } from './lending/lending.service';
import { LendingModule } from './lending/lending.module';
import { LoggerMiddleware } from './common/middleware/logger.middleware';
import { Lending } from './lending/lending.entity';
import { AddressModule } from './address/address.module';
import { Address } from './address/address.entity';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { User } from './users/user.entity';
import { DepositModule } from './deposit/deposit.module';
import { Deposit } from './deposit/deposit.entity';
import { ClaimsModule } from './claims/claims.module';
import { WalletModule } from './wallet/wallet.module';

@Module({
  imports: [ LendingModule, AddressModule, DepositModule, AuthModule, UsersModule, ClaimsModule,
    TypeOrmModule.forRoot(),
    ClaimsModule,
    WalletModule
  ],
  controllers: [AppController],
  providers: [AppService],
  // controllers: [AppController, LendingController],
  // providers: [AppService, LendingService],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: 'lending', method: RequestMethod.GET});
  }
}
